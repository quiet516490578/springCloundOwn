package com.example.server;

import com.example.server.dao.IAgentDao;
import com.example.server.entity.Agent;
import com.example.server.util.JsonMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ServerApplicationTests {

	@Resource
	IAgentDao iAgentDao;

	@Test
	public void contextLoads() {
		Agent agent = iAgentDao.findByAgentNum("A151081809767510160");
		System.out.println("agent=====".concat(JsonMapper.toJsonString(agent)));

	}

}
