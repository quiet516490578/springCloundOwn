package com.example.server.entity.base;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Data
@Entity
public abstract class AbstractEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3620040874194489887L;
	@Id
	private Long id;
	@Column(name = "VERSION")
	private Integer version;
	@Column(name = "DELETE")
	private Integer deleted = 0;
	@Column(name = "CREATE_TIME")
	private Date createTime;
	@Column(name = "MODIFY_TIME")
	private Date modifyTime;
	@Column(name = "REMARK")
	private String remark;

	//以下是数据库没有的
	private String errorCode;
	private String errorMsg;

}
