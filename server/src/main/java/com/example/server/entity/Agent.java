package com.example.server.entity;


import com.example.server.entity.base.AbstractEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by hanzengzhi on 2017/10/20.
 *
 * @Data : 注解在类上, 为类提供读写属性, 此外还提供了 equals()、hashCode()、toString() 方法
 * @NoArgsConstructor, @RequiredArgsConstructor, @AllArgsConstructor : 注解在类上, 为类提供无参,有指定必须参数, 全参构造函数
 *
 */
@Data
@Entity
@Table(name = "pfb_agent")
public class Agent {

    @Id
    private Long id;
    @Column(name = "VERSION")
    private Integer version;
    @Column(name = "DELETE")
    private Integer deleted = 0;
    @Column(name = "CREATE_TIME")
    private Date createTime;
    @Column(name = "MODIFY_TIME")
    private Date modifyTime;
    @Column(name = "REMARK")
    private String remark;

    @Column(name = "AGETN_NUM")
    private String agentNum;                    //代理商编号
    @Column(name = "PUBLIC_KEY")
    private String publicKey;                   // 公钥
    @Column(name = "PRIVATE_KEY")
    private String privateKey;                  // 私钥
    @Column(name = "FULL_NAME")
    private String fullName;                    //全称
    @Column(name = "SHORT_NAME")
    private String shortName;                   //简称
    @Column(name = "FLAG")
    private Long flag;
    @Column(name = "CONTACT_PHONE")
    private String contactPhone;                //联系电话
    @Column(name = "EMAIL")
    private String email;                       //邮箱
    @Column(name = "DEFAULT_CUSTOMER_INFO")
    private String defaultCustomerInfo;         //商户的默认信息
    @Column(name = "CALL_BACK_URL")
    private String callBackUrl;                 //回调地址
    @Column(name = "LEVEL")
    private int    level;                       //等级

}
