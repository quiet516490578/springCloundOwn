package com.example.server.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import java.sql.SQLException;

@Configuration
public class DruidDataSourceProperties {

    static final Logger LOGGER = LoggerFactory.getLogger(DruidDataSourceProperties.class);

    @Value("${spring.datasource.url}")
    private String dbUrl;

    @Value("${spring.datasource.username}")
    private String dbUsername;

    @Value("${spring.datasource.password}")
    private String dbPassword;

    @Value("${spring.datasource.driver-class-name}")
    private String driverClassName;

    /**
     * 连接池初始大小
     */
    @Value("${spring.datasource.initialSize}")
    private int initialSize;

    /**
     * 连接池最小大小
     */
    @Value("${spring.datasource.minIdle}")
    private int minIdle;

    /**
     * 连接池最大大小
     */
    @Value("${spring.datasource.maxActive}")
    private int maxActive;

    /**
     * 连接超时时间
     */
    @Value("${spring.datasource.maxWait}")
    private Long maxWait;

    /**
     * 配置监测时间间隔,监测需要关闭空闲连接,时间为毫秒
     */
    @Value("${spring.datasource.timeBetweenEvictionRunsMillis}")
    private Long timeBetweenEvictionRunsMillis;

    /**
     * 配置一个连接池最小生存时间,单位是毫秒
     */
    @Value("${spring.datasource.minEvictableIdleTimeMillis}")
    private Long minEvictableIdleTimeMillis;

    /**
     * 验证数据库连接SQL语句
     */
    @Value("${spring.datasource.validationQuery}")
    private String validationQuery;

    /**
     * 数据库失效连接保证: testWhileIdle
     * testOnBorrow和testOnReturn 在生产环境一般是不开启的,主要是性能考虑
     * alibaba官网文档:
     * https://github.com/alibaba/druid/wiki/%E9%85%8D%E7%BD%AE_DruidDataSource%E5%8F%82%E8%80%83%E9%85%8D%E7%BD%AE?spm=5176.7935156.0.0.7d2b6d7aYWLEAI
     */
    @Value("${spring.datasource.testWhileIdle}")
    private boolean testWhileIdle;

    @Value("${spring.datasource.testOnBorrow}")
    private boolean testOnBorrow;

    @Value("${spring.datasource.testOnReturn}")
    private boolean testOnReturn;

    /**
     * 打开PSCache
     */
    @Value("${spring.datasource.poolPreparedStatements}")
    private boolean poolPreparedStatements;

    /**
     * 指定每个连接上 PSCache 大小
     */
    @Value("${spring.datasource.maxPoolPreparedStatementPerConnectionSize}")
    private int maxPoolPreparedStatementPerConnectionSize;

    /**
     * 通过 connectProperties 属性来打开 mergeSql 功能;慢 SQL 记录
     */
    @Value("${spring.datasource.connectionProperties}")
    private String connectionProperties;

    /**
     * 合并多个DruidDataSource的监控数据
     */
    @Value("${spring.datasource.useGlobalDataSourceStat}")
    private boolean useGlobalDataSourceStat;

    /**
     * 配置监控统计拦截的 filters ,去掉后监控台SQL无法统计, wall 用于防火墙
     */
    @Value("${spring.datasource.filters}")
    private String filters;

    @Bean(name = "dataSourceDruid")
    @Primary
    public DruidDataSource druidDataSource() {
        DruidDataSource druidDataSource = new DruidDataSource();

        druidDataSource.setUrl(dbUrl);
        druidDataSource.setUsername(dbUsername);
        druidDataSource.setPassword(dbPassword);
        druidDataSource.setDriverClassName(driverClassName);
        druidDataSource.setInitialSize(initialSize);
        druidDataSource.setMinIdle(minIdle);
        druidDataSource.setMaxActive(maxActive);
        druidDataSource.setMaxWait(maxWait);
        druidDataSource.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
        druidDataSource.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
        druidDataSource.setValidationQuery(validationQuery);
        druidDataSource.setTestWhileIdle(testWhileIdle);
        druidDataSource.setTestOnBorrow(testOnBorrow);
        druidDataSource.setTestOnReturn(testOnReturn);
        druidDataSource.setPoolPreparedStatements(poolPreparedStatements);
        druidDataSource.setMaxPoolPreparedStatementPerConnectionSize(maxPoolPreparedStatementPerConnectionSize);
        druidDataSource.setConnectionProperties(connectionProperties);
        druidDataSource.setUseGlobalDataSourceStat(useGlobalDataSourceStat);
        try {
            druidDataSource.setFilters(filters);
        } catch (SQLException e) {
            LOGGER.error("<<===== druid configuration initialization filter", e);
        }

        return druidDataSource;
    }
}
