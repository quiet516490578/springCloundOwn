package com.example.server.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import tk.mybatis.spring.annotation.MapperScan;

import javax.sql.DataSource;

@Configuration
@MapperScan(basePackages = "com.example.server.dao")
public class DataSourcePrimaryConfig {

    public static final String BASE_REPOSITORY_PATH = "com.example.myproject.repository";

    public static final String BASE_MAPPER_PATH = "classpath:mybatis/mapper/*.xml";

    public static final String BASE_MODEL_PATH = "com.example.myproject.domain";

    //只是改了这里
    @Bean(name = "dataSourceDruid")
    @ConfigurationProperties("spring.datasource")
    @Primary
    public DruidDataSource druidDataSource() {
        return new DruidDataSource();
    }

//    @Bean(name = "sqlSessionFactoryDruid")
//    @Primary
//    public SqlSessionFactory sqlSessionFactory(@Qualifier("dataSourceDruid") DataSource dataSource) throws Exception {
//        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
//        sqlSessionFactoryBean.setDataSource(dataSource);
//        sqlSessionFactoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources(DataSourcePrimaryConfig.BASE_MAPPER_PATH));
//        return sqlSessionFactoryBean.getObject();
//    }
//
//    @Bean(name = "dataSourceTransactionManagerDruid")
//    @Primary
//    public DataSourceTransactionManager dataSourceTransactionManager(@Qualifier("dataSourceDruid") DataSource dataSource) {
//        return new DataSourceTransactionManager(dataSource);
//    }
//
//    @Bean(name = "sqlSessionTemplateDruid")
//    @Primary
//    public SqlSessionTemplate sqlSessionTemplate(@Qualifier("sqlSessionFactoryDruid") SqlSessionFactory sqlSessionFactory) {
//        return new SqlSessionTemplate(sqlSessionFactory);
//    }
}
