package com.example.server;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "client",fallback=ErrorFailBackService.class)
//@FeignClient(name = "client")
public interface ITestService {
    @RequestMapping(value = "/hi",method = RequestMethod.GET)
    String test(@RequestParam(value = "name") String name);
}
