package com.example.server;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "service")
public interface TestServiceImpl {
    @RequestMapping(value = "/hi3",method = RequestMethod.GET)
    public void test(@RequestParam(value = "name") String name);
}
