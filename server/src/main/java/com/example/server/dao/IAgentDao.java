package com.example.server.dao;


import com.example.server.entity.Agent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by hanzengzhi on 2017/10/20.
 */
@Repository
public interface IAgentDao extends JpaRepository<Agent,Long> {

    /**
     * 查询代理商信息
     * @param agentNum
     * @return
     */
    Agent findByAgentNum(String agentNum);
}
