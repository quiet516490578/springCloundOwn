package com.example.server;

import com.example.server.dao.IAgentDao;
import com.example.server.entity.Agent;
import com.example.server.util.JsonMapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.lang.reflect.Method;

@RestController
public class TestController {

    @Resource
    ITestService iTestService;
    @Resource
    TestServiceImpl testServiceImpl;
    @Resource
    IAgentDao iAgentDao;

    @RequestMapping(value = "/hi1",method = RequestMethod.GET)
    public void test2(@RequestParam String name){
        System.out.println("test2===========");
        testServiceImpl.test(name);
    }
    @RequestMapping(value = "/hi",method = RequestMethod.GET)
    public void test(@RequestParam String name){
        System.out.println("test===========");
        String s = iTestService.test(name);
        System.out.println("相应的返回值=====".concat(s));
    }
    @RequestMapping(value = "/hi3",method = RequestMethod.GET)
    public String test3(@RequestParam String name){

        System.out.println("test3===========");
        //iTestService.test(name);
        return "true";
    }

    @RequestMapping(value = "/testAgent",method = RequestMethod.GET)
    public String testAgent(){
       Agent agent =  iAgentDao.findByAgentNum("A151089935530110164");
       return JsonMapper.toJsonString(agent);
    }
}
